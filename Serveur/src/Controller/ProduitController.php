<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Produit;

// /**
// * @Route("ecommerce/api/v1.0", name="produit")
// */
class ProduitController extends AbstractController
{
    /**
    * Permet d'avoir la liste de tous les produits
    * @Route("/produit", name="liste_produit", methods={"GET"})
    */
    public function listeProduit()
    {
        $repository   = $this->getDoctrine()->getRepository(Produit::class);
        $listeProduit  = $repository->findAll();
        $listeReponse = array();
        foreach ($listeProduit as $produit) {
                $listeReponse[] = array(
                'id'     => $produit->getID(),
                'nom'    => $produit->getNomProduit(),
                'ref'    => $produit->getRefProduit(),
                'prix'   => $produit->getPrix()
            );
        }
        $reponse = new Response();
        $reponse->setContent(json_encode(array("produit"=>$listeReponse)));
        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        return $reponse;
    }

    /**
     * Permet d'avoir la marque d'un produit grâce à son id
     * @Route("/produit/{id}", name="details_produit", methods={"GET"})
     */
    public function detailsProduit($id){
        $repository = $this->getDoctrine()->getRepository(Produit::class);
        $produit = $repository->find($id);
        $marque = $produit->getMarque();
        $reponse = new Response(json_encode(array(
                        'id'     => $produit->getId(),
                        'nom'    => $marque->getNomProduit(),
                        'ref'    => $produit->getRefProduit(),
                        'prix'   => $produit->getPrix(),
                        'marque' => $marque->getNomMarque()
                        ))
                );
        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        return $reponse;
    }

    /**
     * Permet de créer un produit
     * @Route("/produit", name="nouveau_produit", methods={"POST"})
     */
    public function nouveauProduit(Request $request){
        $entityManager = $this->getDoctrine()->getManager();
        $produit = new Produit();
        $body   = json_decode($request->getContent(), true);
        $nom    = $body['nom'];
        $produit->setNomProduit($nom);
        $ref    = $body['ref'];
        $produit->setRefProduit($ref);
        $prix   = $body['prix'];
        $produit->setPrix($prix);
        $entityManager->persist($produit);
        $entityManager->flush();
        $reponse = new Response(json_encode(array(
                        'id'     => $produit->getId(),
                        'nom'    => $produit->getNomProduit()
                        )
                ));
        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        return $reponse;
    }


    /**
     * Permet de supprimer un produit grâce à son id
     * @Route("/produit/{id}", name="suppression_produit", methods={"DELETE"})
     */
    public function suppressionProduit(Request $request, $id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $repository    = $this->getDoctrine()->getRepository(Produit::class);
        $body          = json_decode($request->getContent(), true);
        $produit        = $repository->find($id);
        $entityManager->remove($produit);
                $entityManager->flush();
                $reponse = new Response(json_encode(array(
                        'nom'    => $marque->getNomProduit(),
                        ))
                );
        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "");
        return $reponse;
    }


   /**
     * Permet de modifier le produit grâce à son id
     * @Route("/produit/{id}", name="modification_produit", methods={"PUT"})
     */
    public function modificationProduit(Request $request, $id){
        $entityManager = $this->getDoctrine()->getManager();
        $repository    = $this->getDoctrine()->getRepository(Produit::class);
        $body          = json_decode($request->getContent(), true);
        $nom           = $body['nom'];
        $ref           = $body['ref'];
        $prix          = $body['prix'];
        $produit        = $repository->find($id);
        $produit->setNomProduit($nom);
        $produit->setRefProduit($ref);
        $produit->setPrix($prix);
        $entityManager->persist($produit);
                $entityManager->flush();
                $reponse = new Response(json_encode(array(
                        'id'     => $produit->getId(),
                        'nom'    => $produit->getNomProduit(),
                        ))
                );
        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        return $reponse;
    }
}