<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Marque;

/**
* @Route("ecommerce/api/v1.0", name="marque")
*/
class MarqueController extends AbstractController
{
    /**
     * @Route("", name="marque")
     */
    public function index()
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/MarqueController.php',
        ]);
    }

    /**
    * Permet d'avoir la liste de toutes les marques
    * @Route("/marque", name="liste_marque", methods={"GET"})
    */
    public function listeMarque()
    {
        $repository   = $this->getDoctrine()->getRepository(Marque::class);
        $listeMarque  = $repository->findAll();
        $listeReponse = array();
        foreach ($listeMarque as $marque) {
                $listeReponse[] = array(
                'id'     => $marque->getID(),
                'nom'    => $marque->getNomMarque()
            );
        }
        $reponse = new Response();
        $reponse->setContent(json_encode(array("marque"=>$listeReponse)));
        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        return $reponse;
    }

    /**
     * Permet d'avoir les produits d'une marque grâce à son id
     * @Route("/marque/{id}", name="details_marque", methods={"GET"})
     */
    public function detailsMarque($id){
        $repository = $this->getDoctrine()->getRepository(Marque::class);
        $marque = $repository->find($id);
        $listeProduits = $marque->getProduits();
        $produits = [];
        foreach ($listeProduits as $prod) {
            $produits[] = array(
                "id"    => $prod->getId(),
                "nom du produit" => $prod->getNomProduit(),
            );
        }
        $reponse = new Response(json_encode(array(
                        'id'     => $marque->getId(),
                        'nom'    => $marque->getNomMarque(),
                        'produits' => $produits,
                        ))
                );
        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        return $reponse;
    }

    /**
     * Permet de créer une marque
     * @Route("/marque", name="nouvelle_marque", methods={"POST"})
     */
    public function nouvelleMarque(Request $request){
        $entityManager = $this->getDoctrine()->getManager();
        $marque = new Marque();
        $body   = json_decode($request->getContent(), true);
        $nom    = $body['nom'];
        $marque->setNomMarque($nom);
        $entityManager->persist($marque);
                $entityManager->flush();

        $reponse = new Response(json_encode(array(
                        'id'     => $marque->getId(),
                        'nom'    => $marque->getNomMarque()
                        )
                ));

        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        return $reponse;
    }

    /**
     * Permet de supprimer une marque grâce à son id
     * @Route("/marque/{id}", name="suppression_marque", methods={"DELETE"})
     */
    public function suppressionMarque(Request $request, $id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $repository    = $this->getDoctrine()->getRepository(Marque::class);
        $body          = json_decode($request->getContent(), true);
        $marque        = $repository->find($id);
        $entityManager->remove($marque);
                $entityManager->flush();
                $reponse = new Response(json_encode(array(
                        'nom'    => $marque->getNomMarque(),
                        ))
                );
        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "");
        return $reponse;
    }

    /**
     * Permet de modifier la marque grâce à son id
     * @Route("/marque/{id}", name="modification_marque", methods={"PUT"})
     */
    public function modificationMarque(Request $request, $id){
        $entityManager = $this->getDoctrine()->getManager();
        $repository    = $this->getDoctrine()->getRepository(Marque::class);
        $body          = json_decode($request->getContent(), true);
        $nom           = $body['nom'];
        $marque        = $repository->find($id);
        $marque->setNomMarque($nom);
        $entityManager->persist($marque);
                $entityManager->flush();
                $reponse = new Response(json_encode(array(
                        'id'     => $marque->getId(),
                        'nom'    => $marque->getNomMarque(),
                        ))
                );
        $reponse->headers->set("Content-Type", "application/json");
        $reponse->headers->set("Access-Control-Allow-Origin", "*");
        return $reponse;
    }
}
