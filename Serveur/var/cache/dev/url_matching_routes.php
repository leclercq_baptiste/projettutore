<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/_profiler' => [[['_route' => '_profiler_home', '_controller' => 'web_profiler.controller.profiler::homeAction'], null, null, null, true, false, null]],
        '/_profiler/search' => [[['_route' => '_profiler_search', '_controller' => 'web_profiler.controller.profiler::searchAction'], null, null, null, false, false, null]],
        '/_profiler/search_bar' => [[['_route' => '_profiler_search_bar', '_controller' => 'web_profiler.controller.profiler::searchBarAction'], null, null, null, false, false, null]],
        '/_profiler/phpinfo' => [[['_route' => '_profiler_phpinfo', '_controller' => 'web_profiler.controller.profiler::phpinfoAction'], null, null, null, false, false, null]],
        '/_profiler/open' => [[['_route' => '_profiler_open_file', '_controller' => 'web_profiler.controller.profiler::openAction'], null, null, null, false, false, null]],
        '/ecommerce/api/v1.0' => [[['_route' => 'marquemarque', '_controller' => 'App\\Controller\\MarqueController::index'], null, null, null, false, false, null]],
        '/ecommerce/api/v1.0/marque' => [
            [['_route' => 'marqueliste_marque', '_controller' => 'App\\Controller\\MarqueController::listeMarque'], null, ['GET' => 0], null, false, false, null],
            [['_route' => 'marquenouvelle_marque', '_controller' => 'App\\Controller\\MarqueController::nouvelleMarque'], null, ['POST' => 0], null, false, false, null],
        ],
        '/produit' => [
            [['_route' => 'liste_produit', '_controller' => 'App\\Controller\\ProduitController::listeProduit'], null, ['GET' => 0], null, false, false, null],
            [['_route' => 'nouveau_produit', '_controller' => 'App\\Controller\\ProduitController::nouveauProduit'], null, ['POST' => 0], null, false, false, null],
        ],
        '/admin' => [[['_route' => 'easyadmin', '_controller' => 'EasyCorp\\Bundle\\EasyAdminBundle\\Controller\\EasyAdminController::indexAction'], null, null, null, true, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/_(?'
                    .'|error/(\\d+)(?:\\.([^/]++))?(*:38)'
                    .'|wdt/([^/]++)(*:57)'
                    .'|profiler/([^/]++)(?'
                        .'|/(?'
                            .'|search/results(*:102)'
                            .'|router(*:116)'
                            .'|exception(?'
                                .'|(*:136)'
                                .'|\\.css(*:149)'
                            .')'
                        .')'
                        .'|(*:159)'
                    .')'
                .')'
                .'|/ecommerce/api/v1\\.0/marque/([^/]++)(?'
                    .'|(*:208)'
                .')'
                .'|/produit/([^/]++)(?'
                    .'|(*:237)'
                .')'
            .')/?$}sDu',
    ],
    [ // $dynamicRoutes
        38 => [[['_route' => '_preview_error', '_controller' => 'error_controller::preview', '_format' => 'html'], ['code', '_format'], null, null, false, true, null]],
        57 => [[['_route' => '_wdt', '_controller' => 'web_profiler.controller.profiler::toolbarAction'], ['token'], null, null, false, true, null]],
        102 => [[['_route' => '_profiler_search_results', '_controller' => 'web_profiler.controller.profiler::searchResultsAction'], ['token'], null, null, false, false, null]],
        116 => [[['_route' => '_profiler_router', '_controller' => 'web_profiler.controller.router::panelAction'], ['token'], null, null, false, false, null]],
        136 => [[['_route' => '_profiler_exception', '_controller' => 'web_profiler.controller.exception_panel::body'], ['token'], null, null, false, false, null]],
        149 => [[['_route' => '_profiler_exception_css', '_controller' => 'web_profiler.controller.exception_panel::stylesheet'], ['token'], null, null, false, false, null]],
        159 => [[['_route' => '_profiler', '_controller' => 'web_profiler.controller.profiler::panelAction'], ['token'], null, null, false, true, null]],
        208 => [
            [['_route' => 'marquedetails_marque', '_controller' => 'App\\Controller\\MarqueController::detailsMarque'], ['id'], ['GET' => 0], null, false, true, null],
            [['_route' => 'marquesuppression_marque', '_controller' => 'App\\Controller\\MarqueController::suppressionMarque'], ['id'], ['DELETE' => 0], null, false, true, null],
            [['_route' => 'marquemodification_marque', '_controller' => 'App\\Controller\\MarqueController::modificationMarque'], ['id'], ['PUT' => 0], null, false, true, null],
        ],
        237 => [
            [['_route' => 'details_produit', '_controller' => 'App\\Controller\\ProduitController::detailsProduit'], ['id'], ['GET' => 0], null, false, true, null],
            [['_route' => 'suppression_produit', '_controller' => 'App\\Controller\\ProduitController::suppressionProduit'], ['id'], ['DELETE' => 0], null, false, true, null],
            [['_route' => 'modification_produit', '_controller' => 'App\\Controller\\ProduitController::modificationProduit'], ['id'], ['PUT' => 0], null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
